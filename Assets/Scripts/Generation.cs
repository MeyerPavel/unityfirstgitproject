﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generation : MonoBehaviour {

    [SerializeField]
    private GameObject cube;
    [SerializeField]
    private float spaceBetweenCubes = 2f;

    public Transform randomColor;
	void Start () {
        for(float countX = 0;countX < spaceBetweenCubes * 4; countX+= spaceBetweenCubes)
            for(float countY = 0; countY < spaceBetweenCubes * 4; countY += spaceBetweenCubes)
            {
                GameObject tempCube = Instantiate(cube);
                tempCube.transform.position = new Vector3(countX, 0, countY);
                tempCube.GetComponent<Renderer>().material.color = new Color(Random.value, Random.value, Random.value);                      
            }
	}
}
