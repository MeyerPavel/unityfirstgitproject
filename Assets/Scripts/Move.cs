﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

    // Use this for initialization
    [SerializeField]
    private int speed = 5;
    [SerializeField]
    private int jumpImpulse = 5;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal")*speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetAxisRaw("Vertical") != 0)
        {
            transform.Translate(new Vector3(0, 0, Input.GetAxisRaw("Vertical")*speed * Time.deltaTime));
        }
        if (Input.GetKey(KeyCode.Space))
        {
            Rigidbody rig = GetComponent<Rigidbody>();
            rig.AddForce(new Vector3(0, jumpImpulse, 0), ForceMode.Impulse);
        }
    }
}
