﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour {

    private GameObject MainPlayer = GameObject.FindGameObjectWithTag("MainCube");
    void OnTriggerEnter(Collider other)
    {
        
        MainPlayer.GetComponent<Renderer>().material.color = new Color(1,1,1);
    }
}
